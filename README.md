# PolarDB-practice

#### 1 介绍
PolarDB应用实践, 大家可以在此项目中发issue, 或者提及你的PolarDB最佳实践. 


#### 2 目录结构说明 
```
大学缩写/学院_专业_届/学号_昵称/readme.md 
大学缩写/学院_专业_届/学号_昵称/最佳实践编号001.md 
大学缩写/学院_专业_届/学号_昵称/其他相关文件  
```

以上请全部使用小写字母+数字组合. 

在readme.md文件中建立最佳实践文档索引, 例如:
```
[xxx最佳实践](./最佳实践001.md) 
简短描述该实践适应的应用场景, 相比其他方法的优势, 注意事项等.  
```
  
#### 3 提交步骤
1、注册gitee

2、fork polardb-practice 项目到自己的项目中
https://gitee.com/polardb/polardb-practice

3、新增内容
如果没有你所在大学的目录, 请自行新增. 然后将你的实践内容提交到相应位置并新增readme.md描述文件.

```
大学缩写/学院_专业_届/学号_昵称/readme.md 
大学缩写/学院_专业_届/学号_昵称/最佳实践编号001.md 
大学缩写/学院_专业_届/学号_昵称/其他相关文件  
```

4、commit
在你的项目中提交你的修改. 

5、pr
在你的项目中提交pull request.  

#### 4 发布和领取任务  
任何人都可以在[task](./task)目录中发布和领取任务.  

任务文件命名规则:  
```
yyyymmdd_NUM.md 
```

内容模板:
```
任务标题:

背景:

适用场景:

任务价值:

目标:

检验标准:
```
  
发布任务后, 请将任务标题放入[task/readme.md](task/readme.md)文件中作为引用.  

#### 5 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
